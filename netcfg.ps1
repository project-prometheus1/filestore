﻿#Requires -RunAsAdministrator

function isolate
{
    $IP = "192.168.1.10"
    $MaskBits = 24
    $Gateway = "192.168.1.1"
    $Dns = "192.168.1.1"
    $IPType = "IPv4"
    Write-Host "`nClearing Current Config..."
    $adapter = Get-NetAdapter | ? {$_.Status -eq "up"}
    If (($adapter | Get-NetIPConfiguration).IPv4Address.IPAddress) {
        $adapter | Remove-NetIPAddress -AddressFamily $IPType -Confirm:$false
    }
    If (($adapter | Get-NetIPConfiguration).Ipv4DefaultGateway) {
        $adapter | Remove-NetRoute -AddressFamily $IPType -Confirm:$false
    }
    Write-Host "`nSetting New Config..."
    $adapter | New-NetIPAddress `
        -AddressFamily $IPType `
        -IPAddress $IP `
        -PrefixLength $MaskBits `
        -DefaultGateway $Gateway
    $adapter | Set-DnsClientServerAddress -ServerAddresses $DNS

}

function dhcp
{
    $IPType = "IPv4"
    $adapter = Get-NetAdapter | ? {$_.Status -eq "up"}
    $interface = $adapter | Get-NetIPInterface -AddressFamily $IPType
    If ($interface.Dhcp -eq "Disabled") {
        Write-Host "`nClearing Current Config..."
        If (($interface | Get-NetIPConfiguration).Ipv4DefaultGateway) {
            $interface | Remove-NetRoute -Confirm:$false
        }
        Write-Host "`nSetting New Config..."
        $interface | Set-NetIPInterface -DHCP Enabled
        $interface | Set-DnsClientServerAddress -ResetServerAddresses
    }

}


do {
Write-Host "`n============= Choose Your Network Settings=============="
Write-Host "`t1.  Isolated"
Write-Host "`t2.  DHCP"
Write-Host "`t3.  Exit"
Write-Host "========================================================"
$choice = Read-Host "`nEnter Choice"
} until (($choice -eq '1') -or ($choice -eq '2') -or ($choice -eq '3') )
switch ($choice) {
   '1'{
       Write-Host "`nSetting Network to Isolated"
       isolate
       Write-Host "`nNetwork Changes Complete"
       read-host “Press ENTER to continue...”
   }
   '2'{
      Write-Host "`nSetting Network to DHCP"
      dhcp
      Write-Host "`nNetwork Changes Complete"
      read-host “Press ENTER to continue...”
   }
    '3'{
      Return
   }
}