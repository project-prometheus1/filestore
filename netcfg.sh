#!/bin/bash

#Create function for netplan static

netplanstatic () {
   echo "Setting Static IP info to $2 - $1..."

   cat <<EOT >> 01-netcfg.yaml
   network:
      version: 2
      renderer: networkd
      ethernets:
        enp0s3:
          dhcp4: no
          addresses:
            - $1/24
          gateway4: 192.168.1.1
          nameservers:
            addresses: [192.168.1.1]
EOT
   sudo mv 01-netcfg.yaml /etc/netplan/
   sudo netplan apply
}


#Create function for netplan dhcp

netplandhcp () {
   echo "Setting IP info to DHCP..."

   cat <<EOT >> 01-netcfg.yaml
   network:
      version: 2
      renderer: networkd
      ethernets:
        enp0s3:
          dhcp4: yes
EOT
   sudo mv 01-netcfg.yaml /etc/netplan/
   sudo netplan apply
}

#Prompt for machine

PS3="Choose the System: "

select vm in ODYSSEY SIDR DHCP Quit
do
    echo "Selected System:  $vm"

    if [[ "$vm" == "ODYSSEY" ]] ; then
       ip="192.168.1.1"
       netplanstatic $ip $vm
       echo "Complete..."
       break
    elif [[ "$vm" == "SIDR" ]] ; then
       ip="192.168.1.2"
       netplanstatic $ip $vm
       echo "Complete..."
       break
    elif [[ $vm == "DHCP" ]]; then
       netplandhcp
       echo "Complete..."
       break
    else
       break
    fi

done
